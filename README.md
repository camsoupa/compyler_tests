# Python lexer tests

To run tests:

Modify the included Makefile with your executable information.

`$ cd dir/of/Makefile/and/your/exe`
`$ make test`


To generate a long indent file (in which each line is indented by one more space than previous line):

`$ ruby generate_many_indents.rb <num_lines>`


To generate escape sequences:

`$ ruby generate_ascii_escapes_hex.rb <string_or_byte_prefix>`
`$ ruby generate_ascii_escapes_oct.rb <string_or_byte_prefix>`

where <string_or_byte_prefix> is one of the following:

`b | B | br | Br | bR | BR | rb | rB | Rb | RB | r | u | R | U`

Example for generating a file of ascii raw byte escapes to make sure the escapes are not treated as escapes:

`$ ruby generate_ascii_escapes_oct.rb Br`

This prints the resulting escapes to the command line. 

Then copy the escapes into [Pylex](http://matt.might.net/apps/pylex/) to get your expected output tokens.

Create your `my_prefixed_escapes_.py` and `my_prefixed_escapes.py.expected` in `tests/`
