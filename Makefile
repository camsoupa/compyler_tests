#sample makefile

test: <YOUR_LEXER_EXE_TARGET>
        for test in tests/*.py; do \
            ./<YOUR_LEXER_EXE> < $$test > $$test.actual;  \
            filename=`basename $$test | tr _ ' ' | sed s/\.py//`; \
            result=`diff -q $$test.actual $$test.expected`; \
            if echo "$$result" |grep -q differ; then \
              echo '\033[01;31m'FAIL ... "$$filename"'\033[00;00m';\
              diff -y $$test.actual $$test.expected; \
            else \
              echo '\033[01;32m'PASS ... "$$filename"'\033[00;00m'; \
            fi  ; done
  
clean:
        rm -v -f <YOUR_LEXER_EXE> [ANYTHING_ELSE_TO_REMOVE] tests/*.actual

